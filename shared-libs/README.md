# ECAL automation Jenkins shared libraries
==========================================

This branch contains utilities that are shared by all/many workflows.
The library should be loaded by configuring Jenkins (Manage Jenkins -> Configure System ->  Global Pipeline Libraries). 

The shared library provide a static method `setenv(script, jobname)` that will set
a predefined set of global variables to switch jobs from `dev` to `prod` environments.
This allows one to define a single pipeline for each workflow and simply move from dev to prod by adding `-prod` to the job (Item) name.
