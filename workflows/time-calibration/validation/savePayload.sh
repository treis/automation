#!/bin/bash

RUN_NUMBER=${1}
GT=${2}
ISCC=${3}
OUTDIR=${4}

if [ "$ISCC" == "True" ]
then
    LABEL="CC"
else
    LABEL=" - "
fi

# Get the tag for the given label
TAG=`conddb list $GT | grep 'EcalTimeCalibConstantsRcd' | grep $LABEL | awk '{print $3}'`
echo Look for reference IOV in tag $TAG

# Get the payload for the given tag; the latest payload at the time of the given run
iovs=`conddb list $TAG`

while IFS= read -r line; do
    iov=$(echo $line | awk '{print $1}')
    # strip header rows of conddb list output and only used IOVs, which start with a run number
    if [[ $iov =~ ^[0-9] ]]; then
        if [ $iov -gt $RUN_NUMBER ]
        then
            break
        else
            last_iov=$iov
            last_payload=$(echo $line | awk '{print $4}')
        fi
    fi
done <<< "$iovs"

if [ -n "$last_payload" ]; then
    echo For run $RUN_NUMBER: use the payload $last_payload of IOV $last_iov

    # make output directory for dat files and plots
    mkdir -p $OUTDIR

    # save the payload
    echo $last_payload > $OUTDIR/tmppyload
    exit $?
else
    echo No payload matching run $RUN_NUMBER found in tag $TAG
    exit 1
fi
