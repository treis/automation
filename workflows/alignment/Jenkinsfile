@Library("ectrl-shared-libs")
import static ectrl.vars.GlobalVars.*
setenv(this, env.JOB_BASE_NAME)

pipeline {
    agent {
        label 'lxplus'
    }
    options {
        timeout(time: 6, unit: 'HOURS')
    }
    stages {
        stage('reco-check') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line checks for completed/failed tasks
                    python3 runreco.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE check --max-retries 10 --skipped-delay 14"""
                }
            }
        }
        stage('reco-resubmit') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line is supposed to resubmit failed jobs
                    python3 runreco.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE --eosdir $eospath/alignment/reco/ resubmit --resub-flv 'tomorrow' $lfnoption"""
                }
            }
        }
        stage('reco-submit') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line is supposed to execute/submit the jobs
                    python3 runreco.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE --eosdir $eospath/alignment/reco/ submit --nfiles 10 $t0option $lfnoption"""
                }
            }
        }
        stage('align-check') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line checks for completed/failed tasks
                    python3 runalign.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE check --max-retries 10 --skipped-delay 21"""
                }
            }
        }
        stage('align-resubmit') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line is supposed to resubmit failed jobs
                    python3 runalign.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE --eosdir $eospath/alignment/align/ resubmit --resub-flv 'testmatch'  --template template-align.sub"""
                }
            }
        }
        stage('align-submit') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line is supposed to execute/submit the jobs
                    python3 runalign.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE --eosdir $eospath/alignment/align/ submit --template template-align.sub"""
                }
            }
        }
        stage('align-payload-producer') {
            steps {
                 dir("workflows/alignment") {
                     automationApptainerStep "python3 payload_producer.py --notify=$notify_url --db=$dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE resubmit"
                     automationApptainerStep "python3 payload_producer.py --notify=$notify_url --db=$dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE submit"
                     automationApptainerStep "python3 payload_producer.py --notify=$notify_url --db=$dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE check --max-retries=2 --skipped-delay 21"
                 }
             }
         }
        stage('rereco-check') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line checks for completed/failed tasks
                    python3 runrereco.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE check --max-retries 10 --skipped-delay 21"""
                }
            }
        }
        stage('rereco-resubmit') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line is supposed to resubmit failed jobs
                    python3 runrereco.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE --eosdir $eospath/alignment/rereco/ resubmit --resub-flv 'testmatch' --template template-rereco.sub $lfnoption"""
                }
            }
        }
        stage('rereco-submit') {
            steps {
                dir("workflows/alignment") {
                    automationApptainerStep """
                    # the following line is supposed to execute/submit the jobs
                    python3 runrereco.py --notify $notify_url --db $dbinstance --campaign \$CAMPAIGN --wdir \$CMSSW_BASE --eosdir $eospath/alignment/rereco/ submit --nfiles 5 --template template-rereco.sub $t0option $lfnoption"""
                }
            }
        }

    }
    post {
        unsuccessful {
            sendMattermostNotification "ERROR"
        }
    }
}
