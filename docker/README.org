* Docker images
** Images
The gitlab CI is configured to build the base image for automation. The image
contains a CMSSW release area under =/home/ecalgit= that is configured through
a CI/CD environment variable. Inside the release environment the =ecalautoctrl=
python package is installed.
The environment can be activated with =source /home/ecalgit/setup.sh=.
The CI tags two different set of images: those build from the HEAD of the branch
(last commit) and those from the commit pointed to by the =docker-images-prod= tag.
The former image is tagged as =dev= the latter =prod=.

They can both be used with docker or singularity but they both required CVMFS to be
binded. On lxplus the image can be used with singularity:

#+BEGIN_EXAMPLE
export APPTAINER_CACHEDIR="/tmp/$(whoami)/singularity"
singularity shell --cleanenv -B /eos -B /cvmfs docker://gitlab-registry.cern.ch/cms-ecal-dpg/ecalelfs/automation:online-rctrl-dev
source /home/ecalgit/setup.sh
#+END_EXAMPLE

** Custom ECAL code
The installation of custom ECAL code is performed with the =docker/build.sh= script.

